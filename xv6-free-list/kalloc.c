// The Physical memory allocator, is intended to allocate memory of 4096 byte pages for pipe buffers, kernel stacks, user processes, and page table pages. 
#include "defs.h"
#include "memlayout.h"
#include "mmu.h"
#include "param.h"
#include "spinlock.h"
#include "types.h"

void freerange(void *vstart, void *vend);
extern char end[]; // The first address after kernel is loaded from ELF file. It is defined by the kernel linker script in kernel.ld
/* Node structure in the queue */
struct node {
    struct node *next;
};

/* Implementation of queue data structure */
struct free_page_queue {
    struct node *head;
    struct node *tail;
};

struct {
  struct spinlock lock;
  int use_lock;
  /* Queue to maintain the list of free pages */
  struct free_page_queue fqueue;
} kmem;

// The initialization occurs in 2 phases.
// 1. main() calls kinit1() while it uses entrypgdir to place pages mapped by entrypgdir on free list.
// 2. main() calls kinit2() with rest of the physical pages after installing a full page table mapping them on all cores.

void
kinit1(void *vstart, void *vend) {
  initlock(&kmem.lock, "kmem");
  kmem.use_lock = 0;
  /* Queue data structure is initialized */
  kmem.fqueue.head = kmem.fqueue.tail = 0;
  freerange(vstart, vend);
}

void
kinit2(void *vstart, void *vend) {  
  freerange(vstart, vend);
  kmem.use_lock = 1;
}

void
freerange(void *vstart, void *vend) {
  char *p;
  p = (char*)PGROUNDUP((uint)vstart);
  for(; p + PGSIZE <= (char*)vend; p += PGSIZE)
    kfree(p);
}

//PAGEBREAK: 21
// The page of physical memory pointed at by v is freed, which is normally returned by a call to kalloc(). The exception is when initializing the allocator
void
kfree(char *v)
{
  if((uint)v % PGSIZE || v < end || V2P(v) >= PHYSTOP)
    panic("kfree");

  // Junk is filled to find dangling refs
  memset(v, 1, PGSIZE);
  if(kmem.use_lock)
    acquire(&kmem.lock);
  /* Enqueuing the free page in the queue */

  /* If the queue is initially empty */
  if(kmem.fqueue.head == 0) {
    kmem.fqueue.head = kmem.fqueue.tail = (struct node *)v;
  } 
  else {
    /* if the queue contains atleast one element */
  
    kmem.fqueue.tail->next = (struct node *)v;;
    kmem.fqueue.tail = (struct node *)v;;
  }
  kmem.fqueue.tail->next = 0;
  if(kmem.use_lock)
    release(&kmem.lock);
}

// Allocate one 4096-byte page of physical memory. It returns a pointer that the kernel can use. If memory cannot be allocated, then 0 is returned.
char*
kalloc(void)
{
  struct node *r;
  if(kmem.use_lock)
    acquire(&kmem.lock);

  /* Dequeue operation in queue of free list pages */
  r = kmem.fqueue.head;
  if(r != 0) {
    kmem.fqueue.head = r->next;  
  }
  else {
  
  /* empty queue condition */
    kmem.fqueue.tail = 0;
  }
  if(kmem.use_lock)
    release(&kmem.lock);
  return (char *)r;
}

